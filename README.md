# UML-PlantUML

## Para que serve a ferramenta? 

Crie diagramas UML de forma simples e gratuita a partir do seu navegador, graças ao PlantUML Web Server. Basta inserir um diagrama de texto e obter o resultado no formato PNG ou SVG. Leia mais em:   https://plantuml.com/


## Como posso utilizar a solução? 

## Módulos da Aplicação: 

### A solução é composta por dois módulos ( WAR e JAR )

<b>Arquivo WAR</b>, é uma aplicação web para executar localmente em seu PC. Voce também pode utilizar a ferramenta pela internet (link mais abaixo), entretanto, poderá receber propagandas indesejadas durante a utilização.  

Sugestões:  
1. Baixar o arquivo WAR e executar localmente.
2. Voce pode utilizar dentro do Eclipse. Ver mais informações no site.  

<b>Arquivo JAR</b>, serve para renderizar os diagramas em forma de script. Escolha o diretorio onde estão os scripts e executem o jar. Ao executarem a aplicação gera os arquivos de imagem PNG para documentarem no confluence ouo outra ferrametna de documentação da empresa.

## Como iniciar a aplicação? 

Localize o arquivo .BAT (no projeto), ele é um exemplo de script para chamar os aplicativos WAR e JAR para Windows. Para sistema Linux, voce precisará converter o .bat em .sh. Isso é simples de fazer. Observe exemplo a seguir: 

Arquivo bat para Windows: 

@echo off

java -jar plantuml.jar

...


Arquivo shell para Linux: 

#!/bin/bash

java -jar plantuml.jar

...


## O que preciso para rodar a aplicação localmente? 

Basta ter instalado o JDK 1.8 ou superior para executar localmente as duas aplicações: WAR e JAR. 


## Onde posso obter mais informações sobre a utilização da ferramenta? 

O arquivo PDF que esta neste projeto, é o manual completo de notações que voce pode utilizar. Mas no site voce terá mais informações e exemplos. 

Veja mais sobre a solução em:  https://plantuml.com/sequence-diagram




Divirtam-se, 


