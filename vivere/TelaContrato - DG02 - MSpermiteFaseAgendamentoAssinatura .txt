@startuml
hide footbox
skinparam backgroundColor #EEEBDC
skinparam handwritten false

actor User
User -> TelaContrato : 
[--> TelaContrato : PID session

== Inicializar MS DG02==

TelaContrato -> TelaSubCardsContrato : interações de A-D

TelaSubCardsContrato -> MSpermiteFaseAgendamentoAssinatura : envia PID, \nverifica se possui \ndatas validas para o \nagendamento de assinatura, \nretorna boolean com \npermissão de acesso a tela \nde agendamento.

MSpermiteFaseAgendamentoAssinatura -> MSgetDateRangeContractSignature : obter lista de \ndatas disponiveis \nassintaura

MSpermiteFaseAgendamentoAssinatura -> MSgetDateRangeContractSignature : verifica se esta \nvazia ou nao  (true / false)

TelaSubCardsContrato -> MSAtualizarproposta : se retorno true\n chama MS passando idfase \ne tpacao='A'

TelaSubCardsContrato -> MSAtualizarproposta : se retorno false\n chama MS passando idfase \ne tpacao='R'
@enduml
